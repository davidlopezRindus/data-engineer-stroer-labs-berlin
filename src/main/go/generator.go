package main

import (
	"bytes"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

var usagePattern =
	`
Generate LINE_COUNT random integers (by default 134217728 = 1024MB when represented as 64-bit int).

Usage: %s [-h|--help] [-o FILE_NAME] [LINE_COUNT]

`

func usage(err error) {
	if err != nil {
		fmt.Printf("%s\n", err)
	}
	fmt.Printf(usagePattern, os.Args[0])
	os.Exit(1)
}

func check(err error) {
	if err != nil {
		usage(err)
	}
}

type programData struct {
	outputFile string
	count int
}

func parseArgs(args []string) (programData, error) {
	var outputFile = ""
	var count = 134217728
	var err error
	if len(args) < 2 {
		err = errors.New("no parameters specified")
	}
	for i := 1; i < len(args); i++ {
		if args[i][0] == '-' {
			if args[i] == "-o" {
				i++
				if i >= len(args) {
					usage(errors.New("need a FILE_NAME parameter for -o"))
				}
				outputFile = args[i]
			} else if args[i] == "-h" || args[i] == "--help" {
				usage(nil)
			} else {
				err = errors.New(fmt.Sprintf("unexpected option: %s", args[i]))
				break
			}
		} else {
			count, err = strconv.Atoi(os.Args[i])
		}
	}
	return programData{outputFile: outputFile, count: count}, err
}

func randChunk(newline bool) string {
	if newline {
		return fmt.Sprintf("%d\n", rand.Int())
	}
	return fmt.Sprintf("%d", rand.Int())
}

func main() {
	rand.Seed(time.Now().UnixNano())
	programData, err := parseArgs(os.Args)
	check(err)

	var file *os.File
	if programData.outputFile != "" {
		file, err = os.OpenFile(programData.outputFile, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0755)
		check(err)
	} else {
		file = os.Stdout
	}

	chunk_size := 1000
	chunk_count := programData.count / chunk_size

	for chunk := 0; chunk < chunk_count; chunk++ {
		writer := bytes.NewBufferString("")
		current_chunk := chunk_size
		if chunk == chunk_count {
			chunk_count = programData.count % chunk_size
		}
		for i := 0; i < current_chunk; i++ {
			writer.WriteString(randChunk(chunk < chunk_count || i+1 <= current_chunk))
		}
		_, err := file.WriteString(writer.String())
		check(err)
	}
}