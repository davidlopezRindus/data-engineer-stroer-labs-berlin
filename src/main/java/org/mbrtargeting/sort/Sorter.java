package org.mbrtargeting.sort;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Sorter {

    public void sort(final Path inputPath, final Path outputPath) throws IOException {
        // TODO Add your implementation here.
    }

    public static void main(final String[] args) throws IOException {
        final var sorter = new Sorter();

        if (args.length != 2) {
            System.out.println("Usage: <inputPath> <outputPath>");
            System.exit(1);
        }
        sorter.sort(Paths.get(args[0]), Paths.get(args[1]));
    }

}
