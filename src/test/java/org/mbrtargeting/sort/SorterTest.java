package org.mbrtargeting.sort;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

class SorterTest {

    private Path outputFile;

    @BeforeEach
    void getOutFile(@TempDir final Path tempPath) {
        outputFile = tempPath.resolve("out");
    }

    @AfterEach
    void deleteOutFile() throws IOException {
        if (Files.exists(outputFile)) {
            Files.delete(outputFile);
        }
    }
}
